
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_20:48PM

See merge request itentialopensource/adapters/adapter-keygen!16

---

## 0.5.3 [08-30-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-keygen!14

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_19:05PM

See merge request itentialopensource/adapters/adapter-keygen!13

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_20:20PM

See merge request itentialopensource/adapters/adapter-keygen!12

---

## 0.5.0 [05-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-keygen!11

---

## 0.4.3 [03-27-2024]

* Changes made at 2024.03.27_13:47PM

See merge request itentialopensource/adapters/security/adapter-keygen!10

---

## 0.4.2 [03-11-2024]

* Changes made at 2024.03.11_16:18PM

See merge request itentialopensource/adapters/security/adapter-keygen!9

---

## 0.4.1 [02-27-2024]

* Changes made at 2024.02.27_11:53AM

See merge request itentialopensource/adapters/security/adapter-keygen!8

---

## 0.4.0 [01-05-2024]

* Minor/2023 auto migration

See merge request itentialopensource/adapters/security/adapter-keygen!7

---

## 0.3.0 [01-04-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-keygen!6

---

## 0.2.1 [06-26-2023]

* fix package security issues

See merge request itentialopensource/adapters/security/adapter-keygen!5

---

## 0.2.0 [06-02-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-keygen!3

---

## 0.1.2 [03-08-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/security/adapter-keygen!2

---

## 0.1.1 [12-07-2020]

- Fix the calls in Keygen for the issues found during testing

See merge request itentialopensource/adapters/security/adapter-keygen!1

---
