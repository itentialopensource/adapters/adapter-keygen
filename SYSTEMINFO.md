# Keygen

Vendor: Keygen
Homepage: https://keygen.sh/

Product: Keygen
Product Page: https://keygen.sh/

## Introduction
We classify Keygen into the Security (SASE) domain as it provides software licensing and distribution API.

## Why Integrate
The Keygen adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Keygen. With this adapter you have the ability to perform operations on items such as:

- Users
- Licenses

## Additional Product Documentation
The [Keygen REST API Reference](https://keygen.sh/docs/api/)
