# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Keygen System. The API that was used to build the adapter for Keygen is usually available in the report directory of this adapter. The adapter utilizes the Keygen API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Keygen adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Keygen. With this adapter you have the ability to perform operations on items such as:

- Users
- Licenses

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
