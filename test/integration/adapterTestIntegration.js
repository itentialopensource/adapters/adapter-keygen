/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-keygen',
      type: 'Keygen',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Keygen = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Keygen Adapter Test', () => {
  describe('Keygen Class Tests', () => {
    const a = new Keygen(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-keygen-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-keygen-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const userAccount = 'fakedata';
    const userBodyQuery = {};
    let userId = 'fakedata';
    describe('#postAccountsAccountUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountUser(userAccount, userBodyQuery, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              userId = data.response.id;
              saveMockData('User', 'postAccountsAccountUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAccountsAccountUsersId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountUsersId(userAccount, userId, userBodyQuery, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'postAccountsAccountUsersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAccountsAccountUsersIdActionsResetPassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountUsersIdActionsResetPassword(userAccount, userId, userBodyQuery, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'postAccountsAccountUsersIdActionsResetPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAccountsAccountUsersIdActionsUpdatePassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountUsersIdActionsUpdatePassword(userAccount, userId, userBodyQuery, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'postAccountsAccountUsersIdActionsUpdatePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountsAccountUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountsAccountUser(userAccount, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getAccountsAccountUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountsAccountUserWithOptions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountsAccountUserWithOptions(userAccount, {}, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getAccountsAccountUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountsAccountUserId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountsAccountUserId(userId, userAccount, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getAccountsAccountUserId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licensesAccount = 'fakedata';
    const licensesBodyQuery = {};
    let licensesId = 'fakedata';
    describe('#postAccountsAccountLicenses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountLicenses(licensesAccount, licensesBodyQuery, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.meta);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              licensesId = data.response.id;
              saveMockData('Licenses', 'postAccountsAccountLicenses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountsAccountLicenses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountsAccountLicenses(licensesAccount, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'getAccountsAccountLicenses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAccountsAccountLicensesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchAccountsAccountLicensesId(licensesId, licensesAccount, licensesBodyQuery, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'patchAccountsAccountLicensesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountsAccountLicensesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountsAccountLicensesId(licensesId, licensesAccount, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.meta);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'getAccountsAccountLicensesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licenseActionsAccount = 'fakedata';
    let licenseActionsMeta = {};
    let licenseActionsId = 'fakedata';
    describe('#postAccountsAccountLicensesActionsValidateKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountLicensesActionsValidateKey(licenseActionsAccount, licenseActionsMeta, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.meta);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              licenseActionsMeta = data.response.meta;
              licenseActionsId = data.response.id;
              saveMockData('LicenseActions', 'postAccountsAccountLicensesActionsValidateKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAccountsAccountLicensesIdActionsCheckIn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountLicensesIdActionsCheckIn(licenseActionsId, licenseActionsAccount, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.meta);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LicenseActions', 'postAccountsAccountLicensesIdActionsCheckIn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAccountsAccountLicensesIdActionsDecrementUsage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountLicensesIdActionsDecrementUsage(licenseActionsId, licenseActionsAccount, licenseActionsMeta, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.meta);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LicenseActions', 'postAccountsAccountLicensesIdActionsDecrementUsage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAccountsAccountLicensesIdActionsIncrementUsage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountLicensesIdActionsIncrementUsage(licenseActionsId, licenseActionsAccount, licenseActionsMeta, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.meta);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LicenseActions', 'postAccountsAccountLicensesIdActionsIncrementUsage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAccountsAccountLicensesIdActionsReinstate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountLicensesIdActionsReinstate(licenseActionsId, licenseActionsAccount, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.meta);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LicenseActions', 'postAccountsAccountLicensesIdActionsReinstate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAccountsAccountLicensesIdActionsRenew - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountLicensesIdActionsRenew(licenseActionsId, licenseActionsAccount, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.meta);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LicenseActions', 'postAccountsAccountLicensesIdActionsRenew', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAccountsAccountLicensesIdActionsResetUsage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountLicensesIdActionsResetUsage(licenseActionsId, licenseActionsAccount, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.meta);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LicenseActions', 'postAccountsAccountLicensesIdActionsResetUsage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAccountsAccountLicensesIdActionsRevoke - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postAccountsAccountLicensesIdActionsRevoke(licenseActionsId, licenseActionsAccount, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-keygen-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LicenseActions', 'postAccountsAccountLicensesIdActionsRevoke', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAccountsAccountLicensesIdActionsSuspend - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountLicensesIdActionsSuspend(licenseActionsId, licenseActionsAccount, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.meta);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LicenseActions', 'postAccountsAccountLicensesIdActionsSuspend', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAccountsAccountLicensesIdActionsValidate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountLicensesIdActionsValidate(licenseActionsId, licenseActionsAccount, licenseActionsMeta, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.meta);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LicenseActions', 'postAccountsAccountLicensesIdActionsValidate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountsAccountLicensesIdActionsValidate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountsAccountLicensesIdActionsValidate(licenseActionsId, licenseActionsAccount, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.meta);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LicenseActions', 'getAccountsAccountLicensesIdActionsValidate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let licenseRelationshipsId = 'fakedata';
    const licenseRelationshipsAccount = 'fakedata';
    const licenseRelationshipsData = {};
    describe('#postAccountsAccountLicensesIdTokens - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountLicensesIdTokens(licenseRelationshipsId, licenseRelationshipsAccount, licenseRelationshipsData, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              licenseRelationshipsId = data.response.id;
              saveMockData('LicenseRelationships', 'postAccountsAccountLicensesIdTokens', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putAccountsAccountLicensesIdPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putAccountsAccountLicensesIdPolicy(licenseRelationshipsId, licenseRelationshipsAccount, licenseRelationshipsData, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LicenseRelationships', 'putAccountsAccountLicensesIdPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putAccountsAccountLicensesIdUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putAccountsAccountLicensesIdUser(licenseRelationshipsId, licenseRelationshipsAccount, licenseRelationshipsData, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LicenseRelationships', 'putAccountsAccountLicensesIdUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesAccount = 'fakedata';
    const policiesData = {};
    let policiesId = 'fakedata';
    describe('#postAccountsAccountPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountPolicies(policiesAccount, policiesData, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              policiesId = data.response.id;
              saveMockData('Policies', 'postAccountsAccountPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountsAccountPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountsAccountPolicies(policiesAccount, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getAccountsAccountPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAccountsAccountPoliciesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchAccountsAccountPoliciesId(policiesId, policiesAccount, policiesData, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'patchAccountsAccountPoliciesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountsAccountPoliciesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountsAccountPoliciesId(policiesId, policiesAccount, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getAccountsAccountPoliciesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const productsAccount = 'fakedata';
    const productsData = {};
    let productsId = 'fakedata';
    describe('#postAccountsAccountProducts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountProducts(productsAccount, productsData, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              productsId = data.response.id;
              saveMockData('Products', 'postAccountsAccountProducts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAccountsAccountProductsIdTokens - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountProductsIdTokens(productsId, productsAccount, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Products', 'postAccountsAccountProductsIdTokens', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountsAccountProducts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountsAccountProducts(productsAccount, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Products', 'getAccountsAccountProducts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAccountsAccountProductsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchAccountsAccountProductsId(productsId, productsAccount, productsData, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Products', 'patchAccountsAccountProductsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountsAccountProductsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountsAccountProductsId(productsId, productsAccount, productsData, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Products', 'getAccountsAccountProductsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tokensAccount = 'fakedata';
    let tokensId = 'fakedata';
    const tokensData = {};
    describe('#postAccountsAccountTokens - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountTokens(tokensAccount, tokensData, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              tokensId = data.response.id;
              saveMockData('Tokens', 'postAccountsAccountTokens', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountsAccountMachines - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountsAccountMachines(tokensAccount, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tokens', 'getAccountsAccountMachines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountsAccountMachinesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountsAccountMachinesId(tokensAccount, tokensId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tokens', 'getAccountsAccountMachinesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountsAccountTokens - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountsAccountTokens(tokensAccount, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tokens', 'getAccountsAccountTokens', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putAccountsAccountTokensId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putAccountsAccountTokensId(tokensId, tokensAccount, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tokens', 'putAccountsAccountTokensId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountsAccountTokensId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountsAccountTokensId(tokensId, tokensAccount, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tokens', 'getAccountsAccountTokensId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const machinesAccount = 'fakedata';
    const machinesData = {};
    describe('#postAccountsAccountMachines - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAccountsAccountMachines(machinesAccount, machinesData, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.attributes);
                assert.equal('object', typeof data.response.relationships);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Machines', 'postAccountsAccountMachines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccountsAccountUsersId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAccountsAccountUsersId(userAccount, userId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-keygen-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'deleteAccountsAccountUsersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccountsAccountLicensesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAccountsAccountLicensesId(licensesId, licensesAccount, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-keygen-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'deleteAccountsAccountLicensesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccountsAccountPoliciesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAccountsAccountPoliciesId(policiesId, policiesAccount, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-keygen-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'deleteAccountsAccountPoliciesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccountsAccountPoliciesIdPool - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAccountsAccountPoliciesIdPool(policiesId, policiesAccount, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'deleteAccountsAccountPoliciesIdPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccountsAccountProductsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAccountsAccountProductsId(productsId, productsAccount, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-keygen-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Products', 'deleteAccountsAccountProductsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAccountsAccountTokensId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAccountsAccountTokensId(tokensId, tokensAccount, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-keygen-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tokens', 'deleteAccountsAccountTokensId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
