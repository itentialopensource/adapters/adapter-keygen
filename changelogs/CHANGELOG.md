
## 0.2.1 [06-26-2023]

* fix package security issues

See merge request itentialopensource/adapters/security/adapter-keygen!5

---

## 0.2.0 [06-02-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-keygen!3

---